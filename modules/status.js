const schedule = require('node-schedule');
const cassandra = require('cassandra-driver');
var elasticsearch = require('elasticsearch');
const mqtt = require('mqtt');
const request = require('request');

const async = require('async');
const _ = require('underscore');

var Status = function (app) {

    this.app = app;
    this.server = app.server;
    this.init();


};
module.exports = Status;


Status.prototype.init = function () {

    const self = this;

    var rule = new schedule.RecurrenceRule();
    rule.second = 45;

    var eConn = {}

    var j = schedule.scheduleJob(rule, function () {


        try {

            async.series({
                cassandra: function (ccbk) {

                    const client = new cassandra.Client({
                        contactPoints: self.app.conf.platform.cassandra.machineIp,
                        keyspace: 'boodskapks',
                        policies: {loadBalancing: new cassandra.policies.loadBalancing.RoundRobinPolicy}
                    });

                    client.connect(function (err) {

                        if (!err) {

                            if (self.app.conf.platform.cassandra.machineIp.length != client.hosts.length) {
                                console.log(new Date() + ' | Some Cassandra Cluster is down', client.hosts.keys());
                            }

                            self.app.platformStatus['cassandra'] = {
                                machine: self.app.conf.platform.cassandra.machineIp,
                                runningMachine: client.hosts.keys(),
                                runningCount: client.hosts.length,
                                status: self.app.conf.platform.cassandra.machineIp.length == client.hosts.length ? true : false
                            }
                        } else {
                            console.log(new Date() + ' | Error occurred while Connecting to Cassandra Cluster');
                            self.app.platformStatus['cassandra'] = {
                                machine: self.app.conf.platform.cassandra.machineIp,
                                runningMachine: [],
                                runningCount: 0,
                                status: false
                            }
                        }
                        client.shutdown();
                        ccbk(null, null)
                    })

                },
                elasticsearch: function (ecbk) {

                    var runningMachine = [];

                    async.mapSeries(self.app.conf.platform.elasticSearch.machineIp, function (ip, mCbk) {

                        var req = request.get({
                            uri: "http://"+self.app.conf.platform.elasticSearch.username+":"+self.app.conf.platform.elasticSearch.password+"@"+ip+":"+self.app.conf.platform.elasticSearch.port,
                        }, function (err, res, body) {
                            if (!err) {
                                if (res.statusCode === 200) {
                                    runningMachine.push(ip);

                                } else {
                                    console.log(res.statusCode)
                                    console.log(new Date() + ' | Elastic Cluster IP ' + ip + ' is down');
                                }
                            } else {
                                console.log(err)
                                console.log(new Date() + ' | Elastic Cluster IP ' + ip + ' is down');

                            }
                            req.end()
                            mCbk(null,null);
                        });


                    }, function (err, results) {

                        self.app.platformStatus['elasticSearch'] = {
                            machine: self.app.conf.platform.elasticSearch.machineIp,
                            runningMachine: runningMachine,
                            runningCount: runningMachine.length,
                            status: self.app.conf.platform.elasticSearch.machineIp.length == runningMachine.length ? true : false
                        }
                        ecbk(null, null)

                    })

                },
                emqx: function (emcbk) {

                    async.mapSeries(self.app.conf.platform.emqx.machineIp, function (ip, mCbk) {

                        const options = {
                            connectTimeout: 4000,
                            // Authentication
                            clientId: 'DEV_' + new Date().getTime(),
                            username: 'DEV_' + self.app.conf.platform.adminDomain.domainKey,
                            password: self.app.conf.platform.adminDomain.apiKey,
                            keepalive: 60,
                            clean: true,
                            reconnectPeriod: 0
                        }

                        const TCP_URL = 'mqtt://' + ip + ':' + self.app.conf.platform.emqx.port

                        const mqclient = mqtt.connect(TCP_URL, options)

                        mqclient.on('connect', function (pack) {

                            self.app.platformStatus['emqx'].runningMachine.push(ip);
                            self.app.platformStatus['emqx'].runningMachine = _.uniq(self.app.platformStatus['emqx'].runningMachine)
                            self.app.platformStatus['emqx'].runningCount = self.app.platformStatus['emqx'].runningMachine.length;
                            self.app.platformStatus['emqx'].status = self.app.conf.platform.emqx.machineIp.length == self.app.platformStatus['emqx'].runningMachine.length ? true : false;
                            mqclient.end()
                        })

                        mqclient.on('reconnect', (error) => {
                            console.log(new Date() + " | Error in connecting with EMQX ", ip)
                            mqclient.end()
                        })

                        mqclient.on('error', (error) => {
                            console.log(new Date() + " | Error in connecting with EMQX ", ip)
                            self.app.platformStatus['emqx'].runningMachine = _.difference(self.app.platformStatus['emqx'].runningMachine, [ip])
                            self.app.platformStatus['emqx'].runningCount = self.app.platformStatus['emqx'].runningMachine.length;
                            self.app.platformStatus['emqx'].status = self.app.conf.platform.emqx.machineIp.length == self.app.platformStatus['emqx'].runningMachine.length ? true : false;
                            mqclient.end()
                        })

                        setTimeout(function () {
                            mCbk(null, null);
                        }, 2000)

                    }, function (err, results) {

                        emcbk(null, null)

                    })

                }

            }, function (error, results) {
                self.app.platformStatus['lastUpdated'] = new Date().getTime();

            });

        } catch (e) {
            console.log(new Date() + " | Error Occurred => ", e)
        }
    });


}


Status.prototype.pingStatus = function (req, res) {

    const self = this;

    if (self.app.platformStatus.cassandra && self.app.platformStatus.elasticSearch && self.app.platformStatus.emqx) {
        if (self.app.platformStatus.cassandra.status && self.app.platformStatus.elasticSearch.status
            && self.app.platformStatus.emqx.status) {
            res.status(200).json({status: true, result: self.app.platformStatus})
        } else {
            res.status(417).json({status: false, result: self.app.platformStatus})
        }
    } else {
        res.status(417).json({status: false, result: self.app.platformStatus})
    }
}
