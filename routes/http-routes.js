var Status = require('../modules/status');

var Routes = function (app,router) {

    this.app = app;
    this.router = router;
    this.status = new Status(app);

    this.init();


};
module.exports = Routes;


Routes.prototype.init = function () {

    const self = this;

    self.router.get('/',function (req, res) {
        res.render('home.html',{layout:false,platformStatus:self.app.platformStatus, conf:self.app.conf, basepath:self.app.conf.webUrl+self.app.conf.basepath});
    });

    self.router.get('/ping',function (req, res) {
        self.status.pingStatus(req,res)
    });


    self.app.use(self.app.conf.basepath,self.router);
}
