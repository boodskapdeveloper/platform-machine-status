/*******************************
 * Import Required Modules
 ****************************/
var express = require('express');
var bodyParser = require('body-parser');
const expressSession = require('express-session');
var layout = require('express-layout');
var path = require("path")
var app = express();
var cookieParser = require('cookie-parser')
var session = require('cookie-session');
var compression = require('compression')
var router = express.Router()


/*******************************
 * Require Configuration
 ****************************/
var conf = {};

try {
    conf = require(process.env.HOME + '/config/machine-config');
    console.log(new Date() + ' | Machine Configuration Loaded From Config');
} catch (e) {
    console.log(new Date() + ' | Default Configuration Loaded');
    conf = require('./conf');
}

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


// compress all responses
app.use(compression())

//For Static Files
app.set('views', path.join(__dirname, 'views'));


var options = {
    maxAge: '1d',
    setHeaders: function (res, path, stat) {
        res.set('vary', 'Accept-Encoding');
        res.set('x-timestamp', Date.now());
    }
};
app.use('/css', express.static(__dirname + '/webapps/css', options));
app.use('/images', express.static(__dirname + '/webapps/images', options));
app.use('/fonts', express.static(__dirname + '/webapps/images', options));
app.use('/js', express.static(__dirname + '/webapps/js', options));
app.use(express.static(__dirname + '/webapps', options));

//static url slugs
app.use(conf.basepath,express.static(__dirname + '/webapps', options));
app.use(conf.basepath+'/:a',express.static(__dirname + '/webapps', options));
app.use(conf.basepath+'/:a/:b',express.static(__dirname + '/webapps', options));
app.use(conf.basepath+'/:a/:b/:c',express.static(__dirname + '/webapps', options));
app.use(conf.basepath+'/:a/:b/:c/:d',express.static(__dirname + '/webapps', options));

app.set('base', conf.basepath);

app.use(layout());


//For Template Engine
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set("view options", {layout: "layout.html"});

app.use(cookieParser('1234567890QWERTY'));
var sessionObj = {
    secret: '1234567890QWERTY',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false,
        maxAge: 5 * 60 * 60 * 1000 //5 hours
    }
}
app.set('trust proxy', 1) // trust first proxy
sessionObj.cookie.secure = true // serve secure cookies
app.use(expressSession(sessionObj))


var server = require('http').Server(app,router);


app.conf = conf;
app.platformStatus = {
    startTime : new Date().getTime(),
    cassandra : {
        machine: app.conf.platform.cassandra.machineIp,
        runningMachine: [],
        runningCount: 0,
        status: false
    },
    elasticSearch : {
        machine: app.conf.platform.elasticSearch.machineIp,
        runningMachine: [],
        runningCount: 0,
        status: false
    },
    emqx : {
        machine: app.conf.platform.emqx.machineIp,
        runningMachine: [],
        runningCount: 0,
        status: false
    }
};

console.log("************************************************************");
console.log(new Date() + ' | Platform Machine Status Node Listening on ' + conf['web']['port']);
console.log("************************************************************");

server.listen(conf['web']['port']);

//Initializing the web routes
var Routes = require('./routes/http-routes');
new Routes(app,router);




process
    .on('uncaughtException', function (err) {
        console.error(new Date() + " | ",err)
    })
    .on('unhandledRejection', (reason, p) => {
        console.error(new Date() + " | ",reason, 'Unhandled Rejection at Promise', p);
    })

